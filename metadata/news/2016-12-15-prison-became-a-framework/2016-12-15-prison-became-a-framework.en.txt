Title: kde/prison has been renamed to kde-frameworks/prison
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2016-12-15
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde/prison

Please install kde-frameworks/prison:5 and *afterwards* uninstall kde/prison.

1. Take note of any packages depending on kde/prison:
cave resolve \!kde/prison

2. Install kde-frameworks/prison:
cave resolve kde-frameworks/prison:5 -x

3. Re-install the packages from step 1.

4. Uninstall kde/prison
cave resolve \!kde/prison -x

Do it in *this* order or you'll potentially *break* your system.
