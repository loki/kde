# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}" ] kde [ translations='ki18n' ] gtk-icon-cache

export_exlib_phases src_prepare

SUMMARY="Desktop Planetarium"

LICENCES="GPL-2 FDL-1.2 BSD-3"
SLOT="4"
MYOPTIONS="
    fits    [[ description = [ Support for the F(lexible)I(mage)T(ransport)S(ystem) format ] ]]
    raw     [[ description = [ Support for reading and displaying RAW files ] ]]
    visualisation [[ description = [ Visualise data shown in any of the KStars Image Views ] ]]

    visualisation [[ requires = [ fits ] ]]
"

QT_MIN_VER=5.4.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
        virtual/pkg-config
    build+run:
        kde-frameworks/kauth:5
        kde-frameworks/kconfig:5
        kde-frameworks/kcrash:5
        kde-frameworks/ki18n:5
        kde-frameworks/kio:5
        kde-frameworks/knewstuff:5
        kde-frameworks/knotifications:5
        kde-frameworks/knotifyconfig:5
        kde-frameworks/kplotting:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kxmlgui:5
        sci-libs/eigen:3
        sys-auth/qtkeychain
        sys-libs/zlib
        x11-dri/mesa
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtwebsockets:5[>=${QT_MIN_VER}]
        fits? ( sci-libs/cfitsio )
        raw? ( media-libs/libraw )
        visualisation? ( x11-libs/qtdatavis3d:5 )
    recommendation:
        x11-apps/xplanet [[ description = [
            Renders images of all major planets and most satellites ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Despite the name it requires some deps we don't have. I guess it's
    # intended for usage on mobile devices.
    -DBUILD_KSTARS_LITE:BOOL=FALSE
    # Package Unwritten, http://www.indilib.org; Support for controlling astronomical devices with KStars.
    # Would also need Nova, http://libnova.sourceforge.net/
    -DCMAKE_DISABLE_FIND_PACKAGE_INDI=OFF
    # Package Unwritten, http://www.atnf.csiro.au/people/mcalabre/WCS/; Support for world coordinate
    # systems in FITS header
    -DCMAKE_DISABLE_FIND_PACKAGE_WCSLIB=OFF
    -DUNITY_BUILD:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'fits CFitsio'
    'raw LibRaw'
    'visualisation Qt5DataVisualization'
)

kstars_src_prepare() {
    kde_src_prepare

    # Disable test which needs X (and whose setup is broken)
    edo sed -e "/ADD_TEST(NAME KStarsUiTests COMMAND/d" \
            -i Tests/kstars_ui/CMakeLists.txt
}

