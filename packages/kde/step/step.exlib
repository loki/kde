# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] gtk-icon-cache

SUMMARY="KDE EDU: Interactive Physics Simulator"

LICENCES="GPL-2 FDL-1.2
    ( BSD-2 BSD-3 ) [[ note = [ cmake scripts ] ]]
"
MYOPTIONS="
    gsl-solver [[ description = [ Build a GSL-powered solver ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:* [[ note = [ Needed by xml data translation ] ]]
        kde-frameworks/kdoctools:5
        x11-libs/qttools:5 [[ note = [ lconvert, lrelease (added to tarball) ] ]]
    build+run:
        kde-frameworks/kconfig:5
        kde-frameworks/kcrash:5
        kde-frameworks/kdelibs4support:5
        kde-frameworks/khtml:5
        kde-frameworks/ki18n:5
        kde-frameworks/knewstuff:5
        kde-frameworks/kplotting:5
        sci-libs/eigen:3[>=3.2.2]
        sci-libs/libqalculate:=[>=2.0.0] [[
            note = [ qalculate could be made optional, 2.0.0 to not need cln ]
        ]]
        x11-libs/qtbase:5[>=5.2.0]
        x11-libs/qtsvg:5[>=5.2.0]
        gsl-solver? ( sci-libs/gsl[>=1.8] )
"

# 2 of 2 tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'gsl-solver GSL' )

