# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="LibKSysguard"

LICENCES="GPL-2 LGPL-2.1"
SLOT="4"
MYOPTIONS="
    meminfo [[
        description = [ Displays info about a process's memory usage (needs QtWebkit) ]
    ]]
    X
"

if ever at_least 5.12.90 ; then
    KF5_MIN_VER="[>=5.30.0]"
    QT_MIN_VER="[>=5.5.0]"
fi

DEPENDENCIES="
    build+run:
        kde-frameworks/kauth:5${KF5_MIN_VER}
        kde-frameworks/kcompletion:5${KF5_MIN_VER}
        kde-frameworks/kconfig:5${KF5_MIN_VER}
        kde-frameworks/kconfigwidgets:5${KF5_MIN_VER}
        kde-frameworks/kcoreaddons:5${KF5_MIN_VER}
        kde-frameworks/ki18n:5${KF5_MIN_VER}
        kde-frameworks/kiconthemes:5${KF5_MIN_VER}
        kde-frameworks/kservice:5${KF5_MIN_VER}
        kde-frameworks/kwidgetsaddons:5${KF5_MIN_VER}
        kde-frameworks/kwindowsystem:5${KF5_MIN_VER}
        kde-frameworks/plasma-framework:5${KF5_MIN_VER}
        sys-libs/zlib
        x11-libs/qtbase:5${QT_MIN_VER}
        meminfo? ( x11-libs/qtwebkit:5${QT_MIN_VER} )
        X? (
            kde-frameworks/kwindowsystem:5${KF5_MIN_VER}
            x11-libs/libX11
            x11-libs/libXres
            x11-libs/qtx11extras:5${QT_MIN_VER}
        )
        !kde/kde-workspace[-kf5-coinstall]
"

# 4 of 4 tests need a running X server (5.1.0)
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'meminfo Qt5WebKitWidgets'
    'X X11'
)

