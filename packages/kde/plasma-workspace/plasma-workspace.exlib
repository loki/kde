# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

export_exlib_phases src_prepare

SUMMARY="Plasma libraries and utilities"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 GPL-2 LGPL-2.1"
SLOT="4"
MYOPTIONS="
    gps         [[ description = [ GPS support for geolocation ] ]]
    barcodes    [[ description = [ Create mobile barcodes from clipboard data via prison ] ]]
    file-search [[ description = [ Build the file search runner ] ]]
    geolocation [[ description = [ Data engine to provide location information via wifi or geoip ] ]]
    holidays    [[ description = [ Display holidays in the Plasma calendar ] ]]
    legacy-systray [[ description = [ Makes xembed systrays available in Plasma ] ]]
    qalculate   [[ description = [ Support advanced features of the calculator runner ] ]]
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

if ever at_least 5.12.90 ; then
    KF5_MIN_VER=5.45.0
else
    KF5_MIN_VER=5.42.0
fi
QT_MIN_VER=5.9.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        app-text/iso-codes
        kde/kwin:${SLOT}
        kde/kscreenlocker
        kde/libksysguard:${SLOT}
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}][daemon(+)]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdesu:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=5.7.0]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjsembed:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/krunner:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        media-libs/phonon[>=4.6.60][qt5]
        sys-libs/zlib
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXau
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXfixes
        x11-libs/libXrender
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql] [[ note = [ runners/bookmarks ] ]]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        barcodes? ( kde-frameworks/prison:5[>=${KF5_MIN_VER}] )
        file-search? ( kde-frameworks/baloo:5[>=5.1.95] )
        gps? ( dev-libs/gpsd )
        geolocation? ( kde-frameworks/networkmanager-qt:5[>=${KF5_MIN_VER}] )
        holidays? ( kde-frameworks/kholidays:5 )
        legacy-systray? (
            x11-libs/libXtst
            x11-utils/xcb-util
            x11-utils/xcb-util-image
            !dev-libs/xembed-sni-proxy [[
                description = [ xembed-sni-proxy has been merged into plasma-workspace ]
                resolution = uninstall-blocked-after
            ]]
        )
        qalculate? (
            sci-libs/libqalculate:=[>=2.0.0] [[ note = [ 2.0.0 to not need cln ] ]]
        )
        !kde/kde-runtime[<15.12.1-r1] [[
            description = [ File collisions between ported and unported apps ]
            resolution = uninstall-blocked-after
        ]]
    run:
        kde/drkonqi
        kde/kactivitymanagerd
        kde/milou:${SLOT}          [[ note = [ Needed for krunner ] ]]
        kde/plasma-integration
        kde-frameworks/kded:5
        kde-frameworks/kinit:5
        x11-apps/iceauth
        x11-apps/xmessage      [[ note = [ startkde, startplasma ] ]]
        x11-apps/xprop         [[ note = [ startkde, startplasma ] ]]
        x11-apps/xrdb          [[ note = [ startkde, startplasma ] ]]
        x11-apps/xset          [[ note = [ startkde, startplasma ] ]]
        x11-apps/xsetroot      [[ note = [ startkde, startplasma ] ]]
        x11-libs/qtgraphicaleffects:5
        x11-libs/qtquickcontrols:5
        x11-libs/qttools:5         [[ note = [ startkde, startplasma need qtpaths, qdbus ] ]]
    post:
        kde/kde-cli-tools:${SLOT}  [[ note = [ Needs kde-open5, kcmshell5 ] ]]
        kde/plasma-desktop         [[ note = [ kapplymousetheme used in startkde ] ]]
    suggestion:
        (
            providers:elogind? ( sys-auth/elogind )
            providers:systemd? ( sys-apps/systemd[polkit] )
        ) [[ *description = [ Needed for shutdown/reboot functionality ] ]]
        dev-libs/libappindicator:0.1 [[ description = [ Needed for systray icons of GTK+3 based apps ] ]]
        dev-libs/libappindicator:0.1[gtk2] [[ description = [ Needed for systray icons of GTK+2 based apps ] ]]
        dev-libs/sni-qt [[ description = [ Needed for systray icons of Qt4 based apps ] ]]
        (
            kde/kwallet-pam
            net-misc/socat
        ) [[
            *description = [ Needed to automatically unlock kwallet when logging in ]
            *group-name = [ kwallet-autounlock ]
        ]]
        (
            kde/kwayland-integration:${SLOT}
            x11-libs/qtwayland:5
        ) [[
            *description = [ *Experimental* support for a wayland session ]
            *group-name = [ wayland-session ]
        ]]
        kde/kdeplasma-addons [[ description = [ Collection of new plasmoids ] ]]
        x11-apps/xdg-user-dirs [[ description = [ Create, manage and localize well known directories ] ]]
"

# NOTE: StatusNotifierItem and appmenu use a forked copy of libdbusmenu-qt
#       because of unfixed bugs in the upstream version.

if ever at_least 5.12.90 ; then
    :
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
    "
fi

# 6 of 6 tests need a running X Server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # While we have dev-libs/appstream packaged, we haven't set up any data
    # source, so building the runner based on it seems a bit pointless at
    # the moment.
    -DCMAKE_DISABLE_FIND_PACKAGE_AppStreamQt:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'legacy-systray xembed-sni-proxy' )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'barcodes KF5Prison'
    'file-search KF5Baloo'
    'gps libgps'
    'holidays KF5Holidays'
    Qalculate
    'geolocation KF5NetworkManagerQt'
)
# We might bring the X option back sometime, but right now disabling it is broken.
#    'X X11'

plasma-workspace_src_prepare() {
    kde_src_prepare

    edo sed -e "s:qdbus:qdbus-qt5:" \
        -i startkde/startkde.cmake \
        -i startkde/startplasma.cmake \
        -i startkde/startplasmacompositor.cmake
}

