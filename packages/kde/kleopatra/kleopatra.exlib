# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Certificate manager and GUI for OpenPGP and CMS cryptography"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER="5.47.0"
QT_MIN_VER=5.9.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        app-crypt/gpgme[>=1.8.0][qt5]
        dev-libs/boost[>=1.34.0]
        dev-libs/libassuan[>=2.0.0]
        dev-libs/libgpg-error
        kde/libkleo[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]   [[ note = [ FORCE_DISABLE_KCMUTILS ] ]]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]   [[ note = [ DISABLE_KWATCHGNUPG ] ]]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]   [[ note = [ DISABLE_KWATCHGNUPG ] ]]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        !kde/kdepim[<16.03.80] [[
            description = [ kleopatra has been split out from kdepim ]
            resolution = uninstall-blocked-after
        ]]
        !kde-frameworks/gpgmepp:5 [[
            description = [ gpgme causes a build failure due to similar include paths ]
            resolution = uninstall-blocked-before
        ]]
"

# 1 of 1 test needs a running X server
RESTRICT="test"

kleopatra_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kleopatra_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

