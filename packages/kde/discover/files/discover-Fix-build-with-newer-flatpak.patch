Upstream: yes

From 6ac777ef480af432be0260d3bf723ee06031fa10 Mon Sep 17 00:00:00 2001
From: Aleix Pol <aleixpol@kde.org>
Date: Fri, 8 Jun 2018 16:15:43 +0200
Subject: [PATCH] Fix build with newer flatpak

Flatpak upstream introduced a FlatpakTransaction type that collided with
ours. Renamed ours to FlatpakTransactionJob for now.

CCMAIL: alexl@redhat.com
---
 .../backends/FlatpakBackend/CMakeLists.txt    |  4 +-
 .../FlatpakBackend/FlatpakBackend.cpp         | 16 +++---
 ...nsaction.cpp => FlatpakJobTransaction.cpp} | 56 +++++++++----------
 ...kTransaction.h => FlatpakJobTransaction.h} | 20 +++----
 ...onJob.cpp => FlatpakTransactionThread.cpp} | 24 ++++----
 ...actionJob.h => FlatpakTransactionThread.h} | 10 ++--
 6 files changed, 65 insertions(+), 65 deletions(-)
 rename libdiscover/backends/FlatpakBackend/{FlatpakTransaction.cpp => FlatpakJobTransaction.cpp} (71%)
 rename libdiscover/backends/FlatpakBackend/{FlatpakTransaction.h => FlatpakJobTransaction.h} (80%)
 rename libdiscover/backends/FlatpakBackend/{FlatpakTransactionJob.cpp => FlatpakTransactionThread.cpp} (91%)
 rename libdiscover/backends/FlatpakBackend/{FlatpakTransactionJob.h => FlatpakTransactionThread.h} (88%)

diff --git a/libdiscover/backends/FlatpakBackend/CMakeLists.txt b/libdiscover/backends/FlatpakBackend/CMakeLists.txt
index cc0d0348..4da31f42 100644
--- a/libdiscover/backends/FlatpakBackend/CMakeLists.txt
+++ b/libdiscover/backends/FlatpakBackend/CMakeLists.txt
@@ -7,8 +7,8 @@ set(flatpak-backend_SRCS
     FlatpakBackend.cpp
     FlatpakFetchDataJob.cpp
     FlatpakSourcesBackend.cpp
-    FlatpakTransaction.cpp
-    FlatpakTransactionJob.cpp
+    FlatpakJobTransaction.cpp
+    FlatpakTransactionThread.cpp
 )
 
 add_library(flatpak-backend MODULE ${flatpak-backend_SRCS})
diff --git a/libdiscover/backends/FlatpakBackend/FlatpakBackend.cpp b/libdiscover/backends/FlatpakBackend/FlatpakBackend.cpp
index fc842b3a..f385c949 100644
--- a/libdiscover/backends/FlatpakBackend/FlatpakBackend.cpp
+++ b/libdiscover/backends/FlatpakBackend/FlatpakBackend.cpp
@@ -23,7 +23,7 @@
 #include "FlatpakFetchDataJob.h"
 #include "FlatpakResource.h"
 #include "FlatpakSourcesBackend.h"
-#include "FlatpakTransaction.h"
+#include "FlatpakJobTransaction.h"
 
 #include <resources/StandardBackendUpdater.h>
 #include <resources/SourcesModel.h>
@@ -1188,11 +1188,11 @@ Transaction* FlatpakBackend::installApplication(AbstractResource *app, const Add
         return nullptr;
     }
 
-    FlatpakTransaction *transaction = nullptr;
+    FlatpakJobTransaction *transaction = nullptr;
     FlatpakInstallation *installation = resource->installation();
 
     if (resource->propertyState(FlatpakResource::RequiredRuntime) == FlatpakResource::NotKnownYet && resource->type() == FlatpakResource::DesktopApp) {
-        transaction = new FlatpakTransaction(resource, Transaction::InstallRole, true);
+        transaction = new FlatpakJobTransaction(resource, Transaction::InstallRole, true);
         connect(resource, &FlatpakResource::propertyStateChanged, [resource, transaction, this] (FlatpakResource::PropertyKind kind, FlatpakResource::PropertyState state) {
             if (kind != FlatpakResource::RequiredRuntime) {
                 return;
@@ -1209,13 +1209,13 @@ Transaction* FlatpakBackend::installApplication(AbstractResource *app, const Add
     } else {
         FlatpakResource *runtime = getRuntimeForApp(resource);
         if (runtime && !runtime->isInstalled()) {
-            transaction = new FlatpakTransaction(resource, runtime, Transaction::InstallRole);
+            transaction = new FlatpakJobTransaction(resource, runtime, Transaction::InstallRole);
         } else {
-            transaction = new FlatpakTransaction(resource, Transaction::InstallRole);
+            transaction = new FlatpakJobTransaction(resource, Transaction::InstallRole);
         }
     }
 
-    connect(transaction, &FlatpakTransaction::statusChanged, [this, installation, resource] (Transaction::Status status) {
+    connect(transaction, &FlatpakJobTransaction::statusChanged, [this, installation, resource] (Transaction::Status status) {
         if (status == Transaction::Status::DoneStatus) {
             updateAppState(installation, resource);
         }
@@ -1241,9 +1241,9 @@ Transaction* FlatpakBackend::removeApplication(AbstractResource *app)
     }
 
     FlatpakInstallation *installation = resource->installation();
-    FlatpakTransaction *transaction = new FlatpakTransaction(resource, Transaction::RemoveRole);
+    FlatpakJobTransaction *transaction = new FlatpakJobTransaction(resource, Transaction::RemoveRole);
 
-    connect(transaction, &FlatpakTransaction::statusChanged, [this, installation, resource] (Transaction::Status status) {
+    connect(transaction, &FlatpakJobTransaction::statusChanged, [this, installation, resource] (Transaction::Status status) {
         if (status == Transaction::Status::DoneStatus) {
             updateAppSize(installation, resource);
         }
diff --git a/libdiscover/backends/FlatpakBackend/FlatpakTransaction.cpp b/libdiscover/backends/FlatpakBackend/FlatpakJobTransaction.cpp
similarity index 71%
rename from libdiscover/backends/FlatpakBackend/FlatpakTransaction.cpp
rename to libdiscover/backends/FlatpakBackend/FlatpakJobTransaction.cpp
index e80d4374..b89df4d8 100644
--- a/libdiscover/backends/FlatpakBackend/FlatpakTransaction.cpp
+++ b/libdiscover/backends/FlatpakBackend/FlatpakJobTransaction.cpp
@@ -19,10 +19,10 @@
  *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
  ***************************************************************************/
 
-#include "FlatpakTransaction.h"
+#include "FlatpakJobTransaction.h"
 #include "FlatpakBackend.h"
 #include "FlatpakResource.h"
-#include "FlatpakTransactionJob.h"
+#include "FlatpakTransactionThread.h"
 
 #include <QDebug>
 #include <QTimer>
@@ -33,12 +33,12 @@ extern "C" {
 #include <glib.h>
 }
 
-FlatpakTransaction::FlatpakTransaction(FlatpakResource *app, Role role, bool delayStart)
-    : FlatpakTransaction(app, nullptr, role, delayStart)
+FlatpakJobTransaction::FlatpakJobTransaction(FlatpakResource *app, Role role, bool delayStart)
+    : FlatpakJobTransaction(app, nullptr, role, delayStart)
 {
 }
 
-FlatpakTransaction::FlatpakTransaction(FlatpakResource *app, FlatpakResource *runtime, Transaction::Role role, bool delayStart)
+FlatpakJobTransaction::FlatpakJobTransaction(FlatpakResource *app, FlatpakResource *runtime, Transaction::Role role, bool delayStart)
     : Transaction(app->backend(), app, role, {})
     , m_app(app)
     , m_runtime(runtime)
@@ -47,11 +47,11 @@ FlatpakTransaction::FlatpakTransaction(FlatpakResource *app, FlatpakResource *ru
     setStatus(QueuedStatus);
 
     if (!delayStart) {
-        QTimer::singleShot(0, this, &FlatpakTransaction::start);
+        QTimer::singleShot(0, this, &FlatpakJobTransaction::start);
     }
 }
 
-FlatpakTransaction::~FlatpakTransaction()
+FlatpakJobTransaction::~FlatpakJobTransaction()
 {
     for(auto job : m_jobs) {
         if (!job->isFinished()) {
@@ -61,48 +61,48 @@ FlatpakTransaction::~FlatpakTransaction()
     }
 }
 
-void FlatpakTransaction::cancel()
+void FlatpakJobTransaction::cancel()
 {
     Q_ASSERT(m_appJob);
-    foreach (const QPointer<FlatpakTransactionJob> &job, m_jobs) {
+    foreach (const QPointer<FlatpakTransactionThread> &job, m_jobs) {
         job->cancel();
     }
     setStatus(CancelledStatus);
 }
 
-void FlatpakTransaction::setRuntime(FlatpakResource *runtime)
+void FlatpakJobTransaction::setRuntime(FlatpakResource *runtime)
 {
     m_runtime = runtime;
 }
 
-void FlatpakTransaction::start()
+void FlatpakJobTransaction::start()
 {
     setStatus(CommittingStatus);
     if (m_runtime) {
-        QPointer<FlatpakTransactionJob> job = new FlatpakTransactionJob(m_runtime, {}, role());
-        connect(job, &FlatpakTransactionJob::finished, this, &FlatpakTransaction::onJobFinished);
-        connect(job, &FlatpakTransactionJob::progressChanged, this, &FlatpakTransaction::onJobProgressChanged);
+        QPointer<FlatpakTransactionThread> job = new FlatpakTransactionThread(m_runtime, {}, role());
+        connect(job, &FlatpakTransactionThread::finished, this, &FlatpakJobTransaction::onJobFinished);
+        connect(job, &FlatpakTransactionThread::progressChanged, this, &FlatpakJobTransaction::onJobProgressChanged);
         m_jobs << job;
 
         processRelatedRefs(m_runtime);
     }
 
     // App job will be added everytime
-    m_appJob = new FlatpakTransactionJob(m_app, {}, role());
-    connect(m_appJob, &FlatpakTransactionJob::finished, this, &FlatpakTransaction::onJobFinished);
-    connect(m_appJob, &FlatpakTransactionJob::progressChanged, this, &FlatpakTransaction::onJobProgressChanged);
+    m_appJob = new FlatpakTransactionThread(m_app, {}, role());
+    connect(m_appJob, &FlatpakTransactionThread::finished, this, &FlatpakJobTransaction::onJobFinished);
+    connect(m_appJob, &FlatpakTransactionThread::progressChanged, this, &FlatpakJobTransaction::onJobProgressChanged);
     m_jobs << m_appJob;
 
     processRelatedRefs(m_app);
 
 
     // Now start all the jobs together
-    foreach (const QPointer<FlatpakTransactionJob> &job, m_jobs) {
+    foreach (const QPointer<FlatpakTransactionThread> &job, m_jobs) {
         job->start();
     }
 }
 
-void FlatpakTransaction::processRelatedRefs(FlatpakResource* resource)
+void FlatpakJobTransaction::processRelatedRefs(FlatpakResource* resource)
 {
     g_autoptr(GPtrArray) refs = nullptr;
     g_autoptr(GError) error = nullptr;
@@ -138,9 +138,9 @@ void FlatpakTransaction::processRelatedRefs(FlatpakResource* resource)
         for (uint i = 0; i < refs->len; i++) {
             FlatpakRef *flatpakRef = FLATPAK_REF(g_ptr_array_index(refs, i));
             if (flatpak_related_ref_should_download(FLATPAK_RELATED_REF(flatpakRef))) {
-                QPointer<FlatpakTransactionJob> job = new FlatpakTransactionJob(resource, QPair<QString, uint>(QString::fromUtf8(flatpak_ref_get_name(flatpakRef)), flatpak_ref_get_kind(flatpakRef)), role());
-                connect(job, &FlatpakTransactionJob::finished, this, &FlatpakTransaction::onJobFinished);
-                connect(job, &FlatpakTransactionJob::progressChanged, this, &FlatpakTransaction::onJobProgressChanged);
+                QPointer<FlatpakTransactionThread> job = new FlatpakTransactionThread(resource, QPair<QString, uint>(QString::fromUtf8(flatpak_ref_get_name(flatpakRef)), flatpak_ref_get_kind(flatpakRef)), role());
+                connect(job, &FlatpakTransactionThread::finished, this, &FlatpakJobTransaction::onJobFinished);
+                connect(job, &FlatpakTransactionThread::progressChanged, this, &FlatpakJobTransaction::onJobProgressChanged);
                 // Add to the list of all jobs
                 m_jobs << job;
             }
@@ -148,9 +148,9 @@ void FlatpakTransaction::processRelatedRefs(FlatpakResource* resource)
     }
 }
 
-void FlatpakTransaction::onJobFinished()
+void FlatpakJobTransaction::onJobFinished()
 {
-    FlatpakTransactionJob *job = static_cast<FlatpakTransactionJob*>(sender());
+    FlatpakTransactionThread *job = static_cast<FlatpakTransactionThread*>(sender());
 
     if (job != m_appJob) {
         if (!job->result()) {
@@ -165,7 +165,7 @@ void FlatpakTransaction::onJobFinished()
         }
     }
 
-    foreach (const QPointer<FlatpakTransactionJob> &job, m_jobs) {
+    foreach (const QPointer<FlatpakTransactionThread> &job, m_jobs) {
         if (job->isRunning()) {
             return;
         }
@@ -175,21 +175,21 @@ void FlatpakTransaction::onJobFinished()
     finishTransaction();
 }
 
-void FlatpakTransaction::onJobProgressChanged(int progress)
+void FlatpakJobTransaction::onJobProgressChanged(int progress)
 {
     Q_UNUSED(progress);
 
     int total = 0;
 
     // Count progress from all the jobs
-    foreach (const QPointer<FlatpakTransactionJob> &job, m_jobs) {
+    foreach (const QPointer<FlatpakTransactionThread> &job, m_jobs) {
         total += job->progress();
     }
 
     setProgress(total / m_jobs.count());
 }
 
-void FlatpakTransaction::finishTransaction()
+void FlatpakJobTransaction::finishTransaction()
 {
     if (m_appJob->result()) {
         AbstractResource::State newState = AbstractResource::None;
diff --git a/libdiscover/backends/FlatpakBackend/FlatpakTransaction.h b/libdiscover/backends/FlatpakBackend/FlatpakJobTransaction.h
similarity index 80%
rename from libdiscover/backends/FlatpakBackend/FlatpakTransaction.h
rename to libdiscover/backends/FlatpakBackend/FlatpakJobTransaction.h
index 1c6562b0..f07acf96 100644
--- a/libdiscover/backends/FlatpakBackend/FlatpakTransaction.h
+++ b/libdiscover/backends/FlatpakBackend/FlatpakJobTransaction.h
@@ -19,8 +19,8 @@
  *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
  ***************************************************************************/
 
-#ifndef FLATPAKTRANSACTION_H
-#define FLATPAKTRANSACTION_H
+#ifndef FLATPAKJOBTRANSACTION_H
+#define FLATPAKJOBTRANSACTION_H
 
 #include <Transaction/Transaction.h>
 #include <QPointer>
@@ -32,15 +32,15 @@ extern "C" {
 }
 
 class FlatpakResource;
-class FlatpakTransactionJob;
-class FlatpakTransaction : public Transaction
+class FlatpakTransactionThread;
+class FlatpakJobTransaction : public Transaction
 {
 Q_OBJECT
 public:
-    FlatpakTransaction(FlatpakResource *app, Role role, bool delayStart = false);
-    FlatpakTransaction(FlatpakResource *app, FlatpakResource *runtime, Role role, bool delayStart = false);
+    FlatpakJobTransaction(FlatpakResource *app, Role role, bool delayStart = false);
+    FlatpakJobTransaction(FlatpakResource *app, FlatpakResource *runtime, Role role, bool delayStart = false);
 
-    ~FlatpakTransaction();
+    ~FlatpakJobTransaction();
 
     void cancel() override;
     void setRuntime(FlatpakResource *runtime);
@@ -57,8 +57,8 @@ private:
 
     QPointer<FlatpakResource> m_app;
     QPointer<FlatpakResource> m_runtime;
-    QPointer<FlatpakTransactionJob> m_appJob;
-    QList<QPointer<FlatpakTransactionJob> > m_jobs;
+    QPointer<FlatpakTransactionThread> m_appJob;
+    QList<QPointer<FlatpakTransactionThread> > m_jobs;
 };
 
-#endif // FLATPAKTRANSACTION_H
+#endif // FLATPAKJOBTRANSACTION_H
diff --git a/libdiscover/backends/FlatpakBackend/FlatpakTransactionJob.cpp b/libdiscover/backends/FlatpakBackend/FlatpakTransactionThread.cpp
similarity index 91%
rename from libdiscover/backends/FlatpakBackend/FlatpakTransactionJob.cpp
rename to libdiscover/backends/FlatpakBackend/FlatpakTransactionThread.cpp
index 20816c28..c8f12938 100644
--- a/libdiscover/backends/FlatpakBackend/FlatpakTransactionJob.cpp
+++ b/libdiscover/backends/FlatpakBackend/FlatpakTransactionThread.cpp
@@ -18,7 +18,7 @@
  *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
  ***************************************************************************/
 
-#include "FlatpakTransactionJob.h"
+#include "FlatpakTransactionThread.h"
 #include "FlatpakResource.h"
 
 #include <QDebug>
@@ -28,7 +28,7 @@ static void flatpakInstallationProgressCallback(const gchar *stats, guint progre
     Q_UNUSED(estimating);
     Q_UNUSED(stats);
 
-    FlatpakTransactionJob *transactionJob = (FlatpakTransactionJob*)userData;
+    FlatpakTransactionThread *transactionJob = (FlatpakTransactionThread*)userData;
     if (!transactionJob) {
         return;
     }
@@ -36,7 +36,7 @@ static void flatpakInstallationProgressCallback(const gchar *stats, guint progre
     transactionJob->setProgress(progress);
 }
 
-FlatpakTransactionJob::FlatpakTransactionJob(FlatpakResource *app, const QPair<QString, uint> &relatedRef, Transaction::Role role)
+FlatpakTransactionThread::FlatpakTransactionThread(FlatpakResource *app, const QPair<QString, uint> &relatedRef, Transaction::Role role)
     : QThread()
     , m_result(false)
     , m_progress(0)
@@ -48,17 +48,17 @@ FlatpakTransactionJob::FlatpakTransactionJob(FlatpakResource *app, const QPair<Q
     m_cancellable = g_cancellable_new();
 }
 
-FlatpakTransactionJob::~FlatpakTransactionJob()
+FlatpakTransactionThread::~FlatpakTransactionThread()
 {
     g_object_unref(m_cancellable);
 }
 
-void FlatpakTransactionJob::cancel()
+void FlatpakTransactionThread::cancel()
 {
     g_cancellable_cancel(m_cancellable);
 }
 
-void FlatpakTransactionJob::run()
+void FlatpakTransactionThread::run()
 {
     g_autoptr(GError) localError = nullptr;
     g_autoptr(FlatpakInstalledRef) ref = nullptr;
@@ -144,22 +144,22 @@ void FlatpakTransactionJob::run()
     setProgress(100);
 }
 
-FlatpakResource * FlatpakTransactionJob::app() const
+FlatpakResource * FlatpakTransactionThread::app() const
 {
     return m_app;
 }
 
-bool FlatpakTransactionJob::isRelated() const
+bool FlatpakTransactionThread::isRelated() const
 {
     return !m_relatedRef.isEmpty();
 }
 
-int FlatpakTransactionJob::progress() const
+int FlatpakTransactionThread::progress() const
 {
     return m_progress;
 }
 
-void FlatpakTransactionJob::setProgress(int progress)
+void FlatpakTransactionThread::setProgress(int progress)
 {
     if (m_progress != progress) {
         m_progress = progress;
@@ -167,12 +167,12 @@ void FlatpakTransactionJob::setProgress(int progress)
     }
 }
 
-QString FlatpakTransactionJob::errorMessage() const
+QString FlatpakTransactionThread::errorMessage() const
 {
     return m_errorMessage;
 }
 
-bool FlatpakTransactionJob::result() const
+bool FlatpakTransactionThread::result() const
 {
     return m_result;
 }
diff --git a/libdiscover/backends/FlatpakBackend/FlatpakTransactionJob.h b/libdiscover/backends/FlatpakBackend/FlatpakTransactionThread.h
similarity index 88%
rename from libdiscover/backends/FlatpakBackend/FlatpakTransactionJob.h
rename to libdiscover/backends/FlatpakBackend/FlatpakTransactionThread.h
index 1ace69ce..34c72356 100644
--- a/libdiscover/backends/FlatpakBackend/FlatpakTransactionJob.h
+++ b/libdiscover/backends/FlatpakBackend/FlatpakTransactionThread.h
@@ -18,8 +18,8 @@
  *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
  ***************************************************************************/
 
-#ifndef FLATPAKTRANSACTIONJOB_H
-#define FLATPAKTRANSACTIONJOB_H
+#ifndef FLATPAKTRANSACTIONTHREAD_H
+#define FLATPAKTRANSACTIONTHREAD_H
 
 extern "C" {
 #include <flatpak.h>
@@ -31,12 +31,12 @@ extern "C" {
 #include <QThread>
 
 class FlatpakResource;
-class FlatpakTransactionJob : public QThread
+class FlatpakTransactionThread : public QThread
 {
 Q_OBJECT
 public:
-    FlatpakTransactionJob(FlatpakResource *app, const QPair<QString, uint> &relatedRef, Transaction::Role role);
-    ~FlatpakTransactionJob() override;
+    FlatpakTransactionThread(FlatpakResource *app, const QPair<QString, uint> &relatedRef, Transaction::Role role);
+    ~FlatpakTransactionThread() override;
 
     void cancel();
     void run() override;
-- 
2.17.1

