# Copyright 2013 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MYPNV="${PN}_gpl-${PV}"
require sourceforge [ project="pyqt/${PN}2/${PNV}" pnv=${MYPNV} suffix=tar.gz ]
require qmake [ slot=5 ]
require python [ blacklist=none multibuild=false with_opt=true ]

SUMMARY="QScintilla is a port to Qt of the Scintilla editing component."
HOMEPAGE="http://www.riverbankcomputing.com/software/qscintilla/"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    python [[ description = [ Build the Python bindings for QScintilla ] ]]
"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:5
        python? ( dev-python/PyQt5[python_abis:*(-)?] )
"
WORK="${WORKBASE}/${MYPNV}"
EQMAKE_SOURCES="${WORK}"/Qt4Qt5/qscintilla.pro
BUGS_TO="kimrhh@exherbo.org"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/qt5_includes.patch
)

src_compile() {
    default

    if option python; then
        edo cd "${WORK}"/Python
        edo ${PYTHON} configure.py \
            -d /$(python_get_sitedir)/PyQt5 \
            --pyqt=PyQt5 \
            --pyqt-sipdir=/usr/share/sip/PyQt5 \
            --qsci-incdir="${WORK}"/Qt4Qt5 \
            --qsci-libdir="${WORK}" \
            --qmake /usr/$(exhost --target)/bin/qmake-qt5

        emake
    fi
}

src_install() {
    default

    if option python; then
        emake -C Python INSTALL_ROOT="${IMAGE}" install
    fi
}

