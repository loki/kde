# Copyright 2011-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm; then
    require kde.org kde
else
    require kde [ api=2 ]
    DOWNLOADS="http://kde.maniatek.com/${PN}/files/${PNV}.tar.xz"
fi

require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE based CDDA extraction tool"
DESCRIPTION="Audex is a audio grabber tool for CD-ROM drives."
HOMEPAGE+=" https://userbase.kde.org/Audex"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        media/cdparanoia
    suggestion:
        dev-python/eyeD3         [[ note = [ eyeD3 will be prefered over LAME for ID3 tagging ] ]]
        media-libs/faac          [[ note = [ Encode mp4/aac files ] ]]
        media-libs/flac          [[ note = [ Encode flac files ] ]]
        media-sound/lame         [[ note = [ Encode mp3 files ] ]]
        media-sound/vorbis-tools [[ note = [ Encode ogg vorbis files ] ]]
"

if ever is_scm; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kcmutils:5
            kde-frameworks/kcompletion:5
            kde-frameworks/kconfig:5
            kde-frameworks/kcoreaddons:5
            kde-frameworks/ki18n:5
            kde-frameworks/kiconthemes:5
            kde-frameworks/kio:5
            kde-frameworks/ktextwidgets:5
            kde-frameworks/kwidgetsaddons:5
            kde-frameworks/kxmlgui:5
            kde-frameworks/solid:5
            kde/libkcddb:5[>=16.11.80]
            x11-libs/qtbase:5
            x11-libs/qtscript:5
            x11-libs/qtx11extras:5
    "
else
    DEPENDENCIES+="
        build+run:
            kde/kdelibs:4
            kde/libkcddb:4[<scm]
            kde/libkcompactdisc:4
    "
fi

audex_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

audex_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

