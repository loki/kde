# Copyright 2008, 2009, 2010, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2008, 2009, 2010 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2013-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases src_configure pkg_postinst pkg_postrm

SUMMARY="Collection of office applications for the K Desktop Environment"
DESCRIPTION="
Office productivity:
* Words: Word processor
* Author: Supports a writer in the creation of books (based on Words)
* Sheets: Spreadsheet calculator
Graphics:
* Karbon: Vector graphics
* Gemini: Calligra adjusted for 2-in-1 devices
* Stage: Presentation program (currently unmaintained, disabled)
* Flow: Flow charts (currently unported, disabled)
* Braindump: Mindmapping tool (currently unmaintained, disabled)
"

LICENCES="GPL-2 LGPL-2 FDL-1.2"
SLOT="2"
MY_KDE_PARTS="gemini karbon sheets words"
# flow - unported
# braindump, stage - unmaintained and disabled for a default build

MYOPTIONS+="
    charts         [[ description = [ Support for embedding charts and diagrams ] ]]
    encrypted      [[ description = [ Support for encrypted OpenDocument files in Words ] ]]
    eps            [[ description = [ EPS (Encapsulated PostScript) import filter for Karbon ] ]]
    jpeg2000       [[ description = [ Support for the JPEG 2000 image format ] ]]
    msworks        [[ description = [ Support for Microsoft Works files in Words ] ]]
    okular         [[ description = [ Build plugin for Okular supporting ODP and MS PPT/PPT documents ] ]]
    openexr        [[ description = [ Support for OpenEXR, a high precision file format for use in computer imaging applications ] ]]
    pdf            [[ description = [ Support for PDF (Portable Document Format) import in Karbon ] ]]
    spacenavigator [[ description = [ Space navigator 3D mouse device plugin ] ]]
    wordperfect    [[ description = [ WordPerfect Document support for Words and Graphics support for Karbon ] ]]

    ( kde_parts: ( ${MY_KDE_PARTS} ) [[ number-selected = at-least-one ]] )
    pdf? (
        ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    )
    wordperfect? ( ( kde_parts: karbon words ) [[ number-selected = at-least-one ]] )

    eps            [[ requires = [ kde_parts: karbon ] ]]
    jpeg2000       [[ requires = [ pdf ] ]]
    msworks        [[ requires = [ kde_parts: words ] ]]
    pdf            [[ requires = [ kde_parts: karbon ] ]]
"
#    apple-keynote  [[ description = [ Support for importing files from Apple's Keynote ] ]]
#    apple-keynote  [[ requires = [ kde_parts: stage ] ]]
#    visio          [[ description = [ Support for importing files from MS Visio in Flow ] requires = [ kde_parts: flow ] ]]

KF5_MIN_VER=5.7.0

DEPENDENCIES+="
    build:
        kde-frameworks/kdoctools:5
        sys-devel/gettext
    build+run:
        dev-lang/perl:*
        dev-libs/boost
        kde-frameworks/attica:5[>=${KF5_MIN_VER}]    [[ note = [ GHNS, could be optional ] ]]
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}] [[ note = [ could be optional ] ]]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/khtml:5[>=${KF5_MIN_VER}]         [[ note = [ could be optional ] ]]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kinit:5[>=${KF5_MIN_VER}] [[ note = [ kf5_add_kdeinit_executable cmake macro ] ]]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kross:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        media-libs/fontconfig [[ note = [ Required to handle exact font size ] ]]
        media-libs/freetype:2 [[ note = [ Required to handle exact font size ] ]]
        media-libs/lcms2[>=2.4] [[ note = [ Required for color management ] ]]
        media-libs/libpng:=
        media-libs/phonon[qt5] [[ note = [ Stage event actions and videoshape plugin ] ]]
        sys-libs/zlib
        x11-dri/glu
        x11-dri/mesa [[ note = [ Required for opengl support in Flake ] ]]
        x11-libs/libX11
        x11-libs/qtbase:5[>=5.3.0][gui]
        x11-libs/qtdeclarative:5[>=5.3.0]
        x11-libs/qtsvg:5[>=5.3.0]
        x11-libs/qttools:5[>=5.3.0]
        x11-libs/qtx11extras:5[>=5.3.0]
        x11-misc/shared-mime-info [[ note = [ filters/libmsooxml/ ] ]]
        charts? ( kde/kdiagram[>=2.6.0] )
        encrypted? ( app-crypt/qca:2[>=2.1.0][qt5] [[ note = [ Support encrypted odf and ooxml (MS Office 2007) files ] ]] )
        kde_parts:gemini? ( dev-scm/libgit2 )
        kde_parts:karbon? (
            eps? ( media-gfx/pstoedit [[ note = [ Required for eps import (PS/PDF to SVG) ] ]] )
            pdf? (
                app-text/poppler[>=0.12.1][qt5] [[ note = [ Karbon PDF import filter ] ]]
                jpeg2000? ( media-libs/OpenJPEG:0 )
                providers:ijg-jpeg? ( media-libs/jpeg:= )
                providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
            )
        )
        kde_parts:sheets? (
            sci-libs/eigen:3[>=3.0]
            sci-libs/gsl[>=1.7]
            x11-libs/qtbase:5[sql] [[ note = [ Optional for Sheets database connection ] ]]
        )
        kde_parts:words? (
            wordperfect? (
                office-libs/libodfgen[>=0.1]
                office-libs/librevenge
                office-libs/libwpd[>=0.10]
                office-libs/libwpg[>=0.3]
            )
            msworks? (
                office-libs/libodfgen[>=0.1]
                office-libs/librevenge
                office-libs/libwps[>=0.4]
            )
        )
        okular? ( kde/okular:4[>=16.11.0] )
        openexr? (
            media-libs/ilmbase
            media-libs/openexr
        )
        spacenavigator? ( sci-libs/libspnav )
    test:
        kde-frameworks/threadweaver:5[>=${KF5_MIN_VER}]
"
#        kde_parts:braindump? (
#            x11-libs/qtwebkit:5 [[ note = [ The webshape plugin makes use of this, dep is automagic ] ]]
#        )

#        kde_parts:stage? (
#            x11-libs/qtwebkit:5
#            apple-keynote? (
#                office-libs/libetonyek[>=0.1]
#                office-libs/librevenge
#            )
#        )

# Tests need X, last checked: 2.7.2
RESTRICT="test"

# FIXME: Is the apidox custom target useful now?

calligra_src_configure() {
    local p cmakeparams=(
        "${CMAKE_SRC_CONFIGURE_PARAMS[@]}"
        -DBUILD_gemini:BOOL=FALSE # marked as staging
        -DCMAKE_DISABLE_FIND_PACKAGE_LibEtonyek:BOOL=TRUE
        -DCMAKE_DISABLE_FIND_PACKAGE_LibRevenge:BOOL=TRUE
        -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Declarative:BOOL=TRUE # Deprecated, needed for gemini
        -DCMAKE_DISABLE_FIND_PACKAGE_Qt5WebKit:BOOL=TRUE
        -DCMAKE_DISABLE_FIND_PACKAGE_Qt5WebKitWidgets:BOOL=TRUE
        -DPACKAGERS_BUILD:BOOL=FALSE # Support multiple CPU arches in one binary - if Vc is present, see README.PACKAGERS
        -DRELEASE_BUILD:BOOL=TRUE
        -DGHNS:BOOL=FALSE # considered broken by upstream atm, needs kde/attica
        -DWITH_Cauchy:BOOL=FALSE # Matlab/Octave formulas in words/stage - unwritten, no releases yet
        -DWITH_Iconv:BOOL=TRUE # Required for MS .doc support
        -DWITH_LCMS2:BOOL=TRUE
        -DWITH_Libgit2:BOOL=FALSE
        -DWITH_LibVisio:BOOL=FALSE   # visio option -> flow (unported)
        -DWITH_Marble:BOOL=FALSE # depends on rdf (Soprano) -> see below
        $(cmake_with charts KChart)
        $(cmake_with charts KGantt)
        $(cmake_with encrypted Qca-qt5)
        $(cmake_with eps PstoeditSvgOutput)
        $(cmake_with kde_parts:gemini Libgit2)
        $(cmake_with kde_parts:sheets Eigen3)
        $(cmake_with kde_parts:sheets GSL)
        $(cmake_with msworks LibRevenge)
        $(cmake_with msworks LibWps)
        $(cmake_with okular Okular5)
        $(cmake_with OpenEXR)
        $(cmake_with pdf Poppler)
        $(cmake_with pdf PopplerXPDFHeaders)
        # Commented out: "push for released (and maintained) Qt5 version of
        # Soprano" - Needed for marble option
        #$(cmake_with rdf Soprano)
        $(cmake_with spacenavigator Spnav)
        $(cmake_with wordperfect LibRevenge)
        $(cmake_with wordperfect LibWpd)
        $(cmake_with wordperfect LibWpg)
    )

    if option kde_parts:karbon && option pdf ; then
        cmakeparams+=(
            -DWITH_JPEG:BOOL=TRUE
            $(cmake_with jpeg2000 OpenJPEG)
        )
    else
        cmakeparams+=( -DWITH_JPEG:BOOL=FALSE -DWITH_OpenJPEG:BOOL=FALSE )
    fi

    if option kde_parts:words && option wordperfect || option msworks ; then
        cmakeparams+=( -DWITH_LibOdfGen:BOOL=TRUE )
    else
        cmakeparams+=( -DWITH_LibOdfGen:BOOL=FALSE )
    fi

    for p in ${MY_KDE_PARTS}; do
        cmakeparams+=( $(cmake_build kde_parts:${p}) )
    done

    ecmake "${cmakeparams[@]}"
}

calligra_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

calligra_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

