# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='qt' ]

SUMMARY="Support for global workspace shortcuts"
DESCRIPTION="
KGlobalAccel allows you to have global accelerators that are independent of
the focused window.  Unlike regular shortcuts, the application's window does not
need focus for them to be activated."

LICENCES="LGPL-2.1"
MYOPTIONS="
    X [[ presumed = true ]]
"

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        X? (
            x11-libs/libX11
            x11-libs/libxcb
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
            x11-utils/xcb-util-keysyms
        )
        !kde/plasma-workspace[<5.2.0-r1] [[
            description = [ kglobalaccel5 was moved into this package from plasma-workspace ]
            resolution = uninstall-blocked-after
        ]]
"

# 1 one of 1 test needs a running X Server (5.7.0)
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'X X11'
    'X XCB'
)

