# Copyright 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='qt' ]

SUMMARY="A stand-alone implementation of the Kate syntax highlighting engine"
DESCRIPTION="
It's meant as a building block for text editors as well as for simple highlighted text rendering
(e.g. as HTML), supporting both integration with a custom editor as well as a ready-to-use
QSyntaxHighlighter sub-class.

It uses Kate syntax definition files for the actual highlighting, the file format is documented
at https://docs.kde.org/stable5/en/applications/katepart/highlight.html.

More than 250 syntax definition files are included, additional ones are picked up from the file
system if present, so you can easily extend this by application-specific syntax definitions for
example."

LICENCES="LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/perl:* [[ note = [ Auto-generate PHP syntax definition files ] ]]
    build+run:
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtxmlpatterns:5[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DKSYNTAXHIGHLIGHTING_USE_GUI:BOOL=TRUE
)

