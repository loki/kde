# Copyright 2010 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2013, 2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install pkg_preinst

MYPNV=${PN}-opensource-src-${PV}

SUMMARY="Qt Creator is a new cross-platform integrated development environment for use with Qt."
HOMEPAGE="https://www.qt.io/"

DOWNLOADS="mirror://qt/official_releases/${PN/-/}/$(ever range 1-2)/${PV}/${MYPNV}.tar.xz"

LICENCES="
    LGPL-2.1 LGPL-3
    BSD-3 [[ note = [ ClassView and ImageViewer plugins ] ]]
    GPL-3 [[ note = [ Qt Quick Desinger ] ]]
    MIT   [[ note = [ Front-end for C++ ] ]]
"

SLOT="0"
MYOPTIONS="
    webengine [[ description = [ Use QtWebEngine instead of a more basic viewer to display the docs  ] ]]
"

min_qt_ver=5.9.0

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-lang/clang:=[>=6.0.0]
        dev-libs/botan:1.10[>=1.10.0]
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/qtbase:5[>=${min_qt_ver}][gui][sql]
        x11-libs/qtdeclarative:5[>=${min_qt_ver}]
        x11-libs/qtquickcontrols:5[>=${min_qt_ver}]
        x11-libs/qtscript:5[>=${min_qt_ver}]
        x11-libs/qtserialport:5[>=${min_qt_ver}]
        x11-libs/qtsvg:5[>=${min_qt_ver}]
        x11-libs/qttools:5[>=${min_qt_ver}]
        x11-libs/qtx11extras:5[>=${min_qt_ver}]
        x11-libs/qtxmlpatterns:5[>=${min_qt_ver}]
        webengine? ( x11-libs/qtwebengine:5[>=${min_qt_ver}] )
    suggestion:
        sys-devel/gdb[python] [[ description = [ For debugging with Qt Creator ] ]]
"

# TODO: Unbundle qbs properly. Simply deleting it doesn't work anymore.

RESTRICT="test" # far to many tests need a running X server, disable building them below

UPSTREAM_DOCUMENTATION="http://doc.qt.io/${PN/-/}/index.html"
UPSTREAM_CHANGELOG="http://code.qt.io/cgit/${PN}/${PN}.git/tree/dist/changes-$(ever range 1-3).md"

BUGS_TO="kimrhh@exherbo.org heirecka@exherbo.org"

ever is_scm || WORK=${WORKBASE}/${MYPNV}

EQMAKE_SOURCES=( qtcreator.pro )
EQMAKE_PARAMS=(
    IDE_PACKAGE_MODE=1
    SUPPORT_QT_QML=1
    USE_SYSTEM_BOTAN=1
    TEST=0
)
DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}"/usr/$(exhost --target) )

qt-creator_src_configure() {
    option webengine || edo sed -e "s/isEmpty(QT\.webenginewidgets\.name)/true/" \
                             -i src/plugins/help/help.pro
    qmake_src_configure
}

qt-creator_src_compile() {
    default
    emake docs
}

qt-creator_src_install(){
    default
    # XXX: Unfortunately there is no way to override datadir. So simply re-organize folders here and
    # add awful symlinks because program location is used to locate resources.
    dodir /usr/share
    edo mv "${IMAGE}"/usr/$(exhost --target)/share/qtcreator "${IMAGE}"/usr/share/

    # Here is awful symlink
    dosym /usr/share/qtcreator /usr/$(exhost --target)/share/

    dosym /usr/$(exhost --target)/bin/qtcreator /usr/$(exhost --target)/bin/qt-creator

    # create .desktop file
    insinto /usr/share/applications
    hereins qtcreator.desktop <<EOF
[Desktop Entry]
Type=Application
Name=Qt Creator
Comment=Complete IDE for Qt applications
Exec=qtcreator
Icon=qtcreator
Categories=Qt;Development;IDE;
EOF
}

qt-creator_pkg_preinst() {
    # Make the update path work
    local dir="${ROOT}/usr/$(exhost --target)/share/qtcreator"
    if [[ -d "${dir}" && ! -L "${dir}" ]] ; then
            nonfatal edo rmdir "${dir}" || ewarn "removing ${dir} failed"
    fi
}

