# Copyright 2014 Niels Ole Salscheider <niels_ole@salscheider-online.de>
# Copyright 2013-2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'qt-4.exlib', which is:
#     Copyright 2008-2010 Bo Ørsted Andresen <zlin@exherbo.org>
#     Copyright 2008-2009, 2010 Ingmar Vanhassel

require qt qmake [ slot=4 ]

SUMMARY="Qt Cross-platform application framework: QtWebkit"

MYOPTIONS="
    media [[ description = [ HTML5 video/audio support via gstreamer ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DOWNLOADS="mirror://kde/stable/${PN}-$(ever range 1-2)/${PV}/src/${PNV}.tar.gz"

SLOT="4"
PLATFORMS="~amd64"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-The-examples-fail-to-install.patch
    "${FILES}"/${PN}-23-Fix-g-5.0-build.patch
)

# parallel builds are broken
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-lang/python:*
        dev-lang/ruby:*
        dev-util/gperf
        sys-devel/bison
        sys-devel/flex
        virtual/pkg-config
    build+run:
        dev-db/sqlite:3
        dev-libs/glib:2
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        media-libs/freetype:2
        media-libs/fontconfig
        media-libs/libpng:=
        media-libs/libwebp:=
        sys-apps/systemd
        sys-libs/zlib
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXext
        x11-libs/libXrender
        x11-libs/qt:4[-webkit(-)]
        media? (
            media-libs/gstreamer:1.0[>=0.10]
            media-plugins/gst-plugins-base:1.0[>=0.10]
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

WORK=${WORKBASE}
EQMAKE_SOURCES=( WebKit.pro )

src_configure() {
    export QMAKEPATH="${WORK}"/Tools/qmake
    export QTDIR="/usr/$(exhost --target)/lib"

    eqmake 4 PKG_CONFIG="${PKG_CONFIG}" $(option media || echo DEFINES+=ENABLE_VIDEO=0)
}

