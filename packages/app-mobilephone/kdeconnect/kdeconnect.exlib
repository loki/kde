# Copyright 2013 Friedrich Kröner <friedrich@mailstation.de>
# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="${PN}-kde"

require kde.org [ subdir=${PN}/${PV}/src ] kde [ translations='ki18n' ]
require test-dbus-daemon freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare src_test pkg_postinst pkg_postrm

SUMMARY="A daemon and KCM to connect your Android device with your desktop"
DESCRIPTION="
Integrate Android with the KDE Desktop.
Current feature list:
- Clipboard share: copy from or to your desktop,
- Notifications sync (4.3+): Read your Android notifications from KDE.
- Multimedia remote control: Use your phone as a remote control.
- WiFi connection: no usb wire or bluetooth needed.
- RSA Encryption: your information is safe.

You can get the matching Android app from:
https://play.google.com/store/apps/details?id=org.kde.kdeconnect_tp
"
HOMEPAGE="https://albertvaka.wordpress.com/
          https://commits.kde.org/${PN}-kde"

LICENCES="GPL-2 || ( GPL-2 GPL-3 )"
SLOT="0"
MYOPTIONS="
    bluetooth [[ description = [ Use kdeconnect with bluetooth connected devices ] ]]
    experimental-app [[ description = [ Build an experimental app, based on Kirigami ] ]]
    wayland   [[ description = [ Integrate with KWayland for fake input support ] ]]
"

KF5_MIN_VER="5.42.0"
QT_MIN_VER="5.7.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-crypt/qca:2[>=2.1.0][qt5(-)] [[ note = [ library and qca-ossl plugin ] ]]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        media-libs/phonon[qt5(-)]
        sys-apps/dbus
        x11-libs/libfakekey
        x11-libs/libX11
        x11-libs/libXtst
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        bluetooth? ( x11-libs/qtconnectivity:5[>=${QT_MIN_VER}] )
        experimental-app? ( kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}] )
        wayland? ( kde-frameworks/kwayland:5[>=${KF5_MIN_VER}] )
    run:
        experimental-app? ( kde-frameworks/kirigami:2 )
    recommendation:
        sys-fs/sshfs-fuse   [[
            description = [ Browse the file system on your mobile device remotely ]
        ]]
        wayland? (
            kde/kwin[wayland(+)] [[
                description = [ KWin's compositor is needed for the fake input interface ]
            ]]
        )
    suggestion:
        app-mobilephone/telepathy-kdeconnect [[
            description = [ Allows sending text messages via Telepathy clients ]
         ]]
        dev-python/nautilus-python [[
            description = [ Add a context menu for sending file via KDE Connect ]
        ]]
        media-sound/pulseaudio [[
            description = [ Mute audio of music/videos during calls via pactl ]
        ]]
"

BUGS_TO="friedrich@mailstation.de heirecka@exherbo.org"

DEFAULT_SRC_PREPARE_PATCHES+=( "${FILES}"/${PN}-tests-Use-QT_GUILESS_MAIN.patch )

CMAKE_SRC_CONFIGURE_PARAMS+=( -DNAUTILUS_PYTHON_EXTENSIONS_INSTALL_DIR=/usr/share/nautilus-python/extensions )
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'bluetooth BLUETOOTH_ENABLED'
    'experimental-app EXPERIMENTALAPP_ENABLED'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'wayland KF5Wayland'
)

kdeconnect_src_prepare() {
    cmake_src_prepare

    # Tries to start kdeinit and other stuff and fails
    edo sed -e "/^ecm_add_test(sendfiletest.cpp/d" -i tests/CMakeLists.txt
}

kdeconnect_src_test() {
    if has_version --slash app-mobilephone/kdeconnect ; then
        ewarn "Skipping tests; they break if you already have kdeconnect installed."
    else
        default
    fi
}

kdeconnect_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kdeconnect_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

