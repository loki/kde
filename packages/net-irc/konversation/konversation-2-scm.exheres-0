# Copyright 2008,2009,2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2014,2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# last checked commit: 1d591297fd3d352fad4041b0f05a7a0d7ef45ed1

require kde.org [ branch="wip/qtquick" ] kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

SUMMARY="Konversation is a graphical Internet Relay Chat client for KDE"
HOMEPAGE="http://konversation.kde.org"

LICENCES="GPL-2 FDL-1.2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

KF5_MIN_VER="5.25.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        app-crypt/qca:2[>=2.1.0][qt5(-)]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kemoticons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        media-libs/phonon[>=4.6.60][qt5]
        x11-libs/qtbase:5[>=5.5.0][gui]
        x11-libs/qtdeclarative:5[>=5.5.0]
    run:
        kde-frameworks/kirigami:2[>=5.39.0]
        kde-frameworks/qqc2-desktop-style[>=5.39.0]
        x11-libs/qtquickcontrols2:5
    recommendation:
        kde/keditbookmarks [[
            description = [ Edit bookmarks via the correspondent menu ]
        ]]
    suggestion:
        dev-lang/python:* [[ description = [ Provides scripting support ] ]]
"

src_install() {
    default

    if ever is_scm ; then
        find "${IMAGE}"/usr/share/locale -type d -empty -delete
    fi
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

